import unittest
import os


class TestLinescount(unittest.TestCase):
    def actualSetUp(self, filename="rascil-base/Dockerfile"):
        cwd = os.getcwd()
        print(cwd)
        self.filename = cwd + "/" + filename

    def _linesCount(self):
        print(self.filename)
        f = open(self.filename)
        number_of_lines = len(f.readlines())
        f.close()
        print(self.filename, number_of_lines)

        assert number_of_lines > 0

    def test_base(self):
        self.actualSetUp(filename="rascil-base/Dockerfile")
        self._linesCount()

    def test_full(self):
        self.actualSetUp(filename="rascil-full/Dockerfile")
        self._linesCount()

    def test_notebook(self):
        self.actualSetUp(filename="rascil-notebook/Dockerfile")
        self._linesCount()


if __name__ == "__main__":
    unittest.main()
