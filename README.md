# Dockerfiles for RASCIL

These Dockerfiles construct RASCIL docker images working from the RASCIL master. 
These currently support building, pushing, and tagging images. The images are named
and tagged as specified in the release file. For example::

    name=rascil-base
    release=0.1.6b0


There are various directories for docker files:

- rascil-base: A minimal RASCIL, without data
- rascil-full: Base with data
- rascil-notebook: Supports running jupyter notebook
- rascil-imaging-qa: Runs the Continuum Imaging QA tool

## Quick start

- `cd` into one of the subdirectories
- Update the version in the release file, before building and pushing the image
- Build the image with `make build`
- Push the image to a docker registry `make push`
- To push the `:latest` tag use `make push_latest`
- To push a version tag without the git SHA use `make push_version` 

## Build, push, and tag a set of Dockerfiles

- `build_all` builds, tags, and pushes all the images
- `rm_all` removes all the images
- `ls_all` lists all the images

## Test the images

The docker/Makefile contains commands for testing all the images. These write results into
the host /tmp area. For docker:

- make test_base
- make test_full
- make test_notebook
- make test_imaging_qa

And for singularity:

- make test_base_singularity
- make test_full_singularity
- make test_notebook_singularity
- make test_imaging_qa_singularity

## Images of RASCIL applications

### Continuum imaging QA tool (a.k.a imaging_qa)

[imaging_qa](Documentation: https://ska-telescope.gitlab.io/external/rascil/apps/imaging_qa.html) finds compact sources in a continuum image and compares them 
to the sources used in the simulation, thus revealing the quality of the imaging.

####DOCKER

Pull the image:

`docker pull nexus.engageska-portugal.pt/rascil-docker/rascil-imaging-qa:latest`

Run the image:

```
docker run -v ${PWD}:/myData -e DOCKER_PATH=${PWD} \
    -e CLI_ARGS='--ingest_fitsname_restored /myData/my_restored.fits \
    --ingest_fitsname_residual /myData/my_residual.fits' \
    --rm nexus.engageska-portugal.pt/rascil-docker/rascil-imaging-qa:latest
```
Run it from the directory where your images you want to check are. The output files will
appear in the same directory. Update the `CLI_ARGS` string with the command line arguments
of the imaging QA code as needed. `DOCKER_PATH` is used to extract the path
of the output files the app produced in your local machine, not in the docker container. This
is used for generating the output file index files.

####SINGULARITY

Pull the image:

`singularity pull rascil-imaging-qa.img docker://nexus.engageska-portugal.pt/rascil-docker/rascil-imaging-qa:latest`

Run the image:

```
singularity run \
    --env CLI_ARGS='--ingest_fitsname_restored test-imaging-pipeline-dask_continuum_imaging_restored.fits \
        --ingest_fitsname_residual test-imaging-pipeline-dask_continuum_imaging_residual.fits' \
    rascil-imaging-qa.img
```
Run it from the directory where your images you want to check are. The output files will
appear in the same directory. If the singularity image you downloaded is in a different path, 
point to that path in the above command. Update the `CLI_ARGS` string with the command line arguments
of the imaging QA code as needed.

####Providing input arguments from a file

You may create a file that contains the input arguments for the app. Here is an example of it,
called `args.txt`::
    
    --ingest_fitsname_restored=/myData/test-imaging-pipeline-dask_continuum_imaging_restored.fits
    --ingest_fitsname_residual=/myData/test-imaging-pipeline-dask_continuum_imaging_residual.fits
    --check_source=True
    --plot_source=True

Make sure each line contains one argument, there is an equal sign between arg and its value,
and that there aren't any trailing white spaces in the lines (and no empty lines). 
The paths to images and other input files has to be the absolute path within the container. 
Here, we use the `DOCKER` example of mounting our data into the `/myData` directory.

Then, calling `docker run` simplifies as::

    docker run -v ${PWD}:/myData  -e DOCKER_PATH=${PWD} -e CLI_ARGS='@/myData/args.txt' \
    --rm nexus.engageska-portugal.pt/rascil-docker/rascil-imaging-qa:latest

Here, we assume that your custom args.txt file is also mounted together with the data into ``/myData``. 
Provide the absolute path to that file when your run the above command.

You can use an args file to run the singularity version with same principles, baring in mind
that singularity will automatically mount your filesystem into the container with paths
matching those on your system.

## Contribute to this repository

We use [Black](https://github.com/psf/black) to keep the python code style in good shape. 

Please make sure you black-formatted your code before merging to master.
